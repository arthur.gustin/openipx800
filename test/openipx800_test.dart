import 'dart:io';

import 'package:http/testing.dart';
import 'package:http/http.dart' show Response ;
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:openipx800/openipx800.dart';

class MockHttpClient extends Mock implements HttpClient {}
class MockHttpClientRequest extends Mock implements HttpClientRequest {}
class MockHttpClientResponse extends Mock implements HttpClientResponse {}
class MockHttpHeaders extends Mock implements HttpHeaders {}

void main() {
  final MockHttpClient client = new MockHttpClient();
  final MockHttpClientRequest request = new MockHttpClientRequest();
  final MockHttpClientResponse response = new MockHttpClientResponse();
  final MockHttpHeaders headers = new MockHttpHeaders();

  group("SetR", () {
    final ipxClient = Ipx800Client(host: "localhost", apikey: "myApiKey", port: 80);
    ipxClient.httpClient = client;

    test('default', () async {
      when(response.statusCode).thenReturn(200);
      when(request.close()).thenAnswer((_) async => await response);
      when(client.getUrl(any)).thenAnswer((_) => new Future<HttpClientRequest>.value(request));

      ipxClient.NewQuery().setRelay([1,2,42]).Do().catchError((s) {
        fail("should not throw error");
      });
      verify(client.getUrl(Uri.parse("http://localhost/api/xdevices.json?SetR=01%2C02%2C42&apikey=myApiKey"))).called(1);
    });

    test('when ipx server returns an error', () async {
      when(response.statusCode).thenReturn(500);
      when(request.close()).thenAnswer((_) async => await response);
      when(client.getUrl(any)).thenAnswer((_) => new Future<HttpClientRequest>.value(request));

      try {
        await ipxClient.NewQuery().setRelay([1,2,42]).Do();
      } catch (e) {
        expect(e, isInstanceOf<IpxApiError>());
      } finally {
        verify(client.getUrl(Uri.parse("http://localhost/api/xdevices.json?SetR=01%2C02%2C42&apikey=myApiKey"))).called(1);
      }
    });

  });

  group("SetVR", () {
    final ipxClient = Ipx800Client(host: "localhost", apikey: "myApiKey", port: 80);
    ipxClient.httpClient = client;
    test('default', () async {
      when(response.statusCode).thenReturn(200);
      when(request.close()).thenAnswer((_) async => await response);
      when(client.getUrl(any)).thenAnswer((_) => new Future<HttpClientRequest>.value(request));

      ipxClient.NewQuery().setVR(1, 50).Do().catchError((s) {
        fail("should not throw error");
      });
      verify(client.getUrl(Uri.parse("http://localhost/api/xdevices.json?SetVR01=50&apikey=myApiKey"))).called(1);

    });
  });

}
