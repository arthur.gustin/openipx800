library openipx800;

import 'dart:io';

class IpxApiError implements Exception {
  String message;
  int statusCode;
  IpxApiError(this.message, this.statusCode);
}

class IpxInvalidInput implements Exception {
  String message;
  IpxInvalidInput(this.message);
}

enum PulseDirection { UP, DOWN }
enum PilotWireMode { COMFORT, ECO, FROST_FREE, STOP}

class IpxRequest {
  Map<String, String> queryParams;
  String host;
  int port;
  String apikey;
  HttpClient httpClient = HttpClient();

  IpxRequest({this.host, this.port, this.apikey, this.httpClient}) {
    this.queryParams = new Map<String, String>();
  }

  IpxRequest setRelay(List<int> relayNumbers) {
    queryParams["SetR"] = relayNumbers
        .map((relayNumber) => relayNumber.toString().padLeft(2, "0"))
        .join(",");
    return this;
  }

  IpxRequest getRelays() {
    queryParams["Get"] = "R";
    return this;
  }

  IpxRequest setVO(List<int> virtualOutputNumbers) {
    queryParams["SetVO"] = virtualOutputNumbers
          .map((relayNumber) => relayNumber.toString().padLeft(3, "0"))
          .join(",");
    return this;
  }

  IpxRequest setVI(List<int> virtualInputNumbers) {
      queryParams["SetVI"] = virtualInputNumbers
          .map((relayNumber) => relayNumber.toString().padLeft(3, "0"))
          .join(",");
      return this;
  }

  IpxRequest setVA(List<int> virtualAnalogInputNumbers, List<int> values) {
    assert (values.length == virtualAnalogInputNumbers.length);

    for (int i = 0; i < virtualAnalogInputNumbers.length; i++) {
      queryParams["SetVA"+virtualAnalogInputNumbers[i].toString().padLeft(2, "0")] = values[i].toString();
    }
    return this;
  }

  static RegExp counterRegex = new RegExp(r"(\+|-){0,1}\d{1,3}");

  IpxRequest setC(List<int> counters, List<String> values) {
    assert (counters.length == values.length);

    for (int i = 0; i < counters.length; i++) {
      if (!counterRegex.hasMatch(values[i])) {
        throw IpxInvalidInput(values[i] + " does not match regex " + counterRegex.toString());
      }
      queryParams["SetC"+counters[i].toString().padLeft(2, "0")] = values[i];
    }

    return this;
  }

  IpxRequest setAllC(String value) {
    if (!counterRegex.hasMatch(value)) {
      throw IpxInvalidInput(value + " does not match regex " + counterRegex.toString());
    }
    queryParams["SetC"] = value;
    return this;
  }

  IpxRequest setVR(int vrNumber, int value) {
    assert (value >= 0 && value <= 100);
    assert (vrNumber >= 0 && vrNumber <= 32);

    queryParams["SetVR"+vrNumber.toString().padLeft(2, "0")] = value.toString();

    return this;
  }

  IpxRequest setAllVR(int value) {
    assert (value >= 0 && value <= 100);

    queryParams["SetVR"] = value.toString();

    return this;
  }

  IpxRequest setDimmer(int numExtension, int dimCha, int dimValue, {int dimDelay = -1}) {
    assert (dimValue >= 0 && dimValue <= 101);
    assert (dimCha >= 1 && dimCha <= 4);
    assert (numExtension >= 1 && numExtension <= 6);

    queryParams["SetDim"] = numExtension.toString();
    queryParams["DimCha"] = dimCha.toString();
    queryParams["DimValue"] = dimValue.toString();

    if (dimDelay > -1) {
      assert (dimDelay >= 0 && dimDelay <= 60000);
      queryParams["DimDelay"] = dimDelay.toString();
    }

    return this;
  }

  IpxRequest setDimAll(int numExtension, int dimValue) {
    assert (numExtension >= 1 && numExtension <= 6);
    assert (dimValue >= 0 && dimValue <= 101);

    queryParams["DimAllOn"] = numExtension.toString();
    queryParams["DimValue"] = dimValue.toString();

    return this;
  }

  // Alias for setVR (VR = volets roulants in french)
  IpxRequest setRS(int rollingShutterNumber, int value) {
    return this.setVR(rollingShutterNumber, value);
  }

  // Alias for setAllVR (VR = volets roulants in french)
  IpxRequest setAllRS(int value) {
   return this.setAllVR(value);
  }

  IpxRequest setPulse(int bsoNumber, int value, PulseDirection direction) {
    assert (bsoNumber > 0 && value <= 32);
    assert (value > 0 && value < 128);

    if (direction == PulseDirection.UP) {
      queryParams["SetPulseUP"+bsoNumber.toString().padLeft(2, "0")] = value.toString();
    } else {
      queryParams["SetPulseDOWN"+bsoNumber.toString().padLeft(2, "0")] = value.toString();
    }

    return this;
  }

  IpxRequest setFp(int zone, PilotWireMode mode) {
    assert (zone >= 1 && zone <= 16 );

    queryParams["SetFP"+zone.toString().padLeft(2, "0")] = mode.index.toString();

    return this;
  }

  IpxRequest setAllFp(PilotWireMode mode) {
    queryParams["SetFP00"] = mode.index.toString();

    return this;
  }

  IpxRequest setEnoPc(int enoNumber) {
    assert (enoNumber > 0 && enoNumber <= 24 );

    queryParams["SetEnoPC"] = enoNumber.toString();

    return this;
  }

  IpxRequest setSms(String telephoneNumber, String message) {
    queryParams["SetSMS"] = telephoneNumber + ":" + message;

    return this;
  }

  IpxRequest setEnoVr(int vrNumber, int value) {
    assert (value >= 0 && value <= 100);
    assert (vrNumber >= 1 && vrNumber <= 24);

    queryParams["SetEnoVR"+vrNumber.toString().padLeft(2, "0")] = value.toString();

    return this;
  }

  // alias for setEnoVr (vr = volets roulants) (rs = rolling shutter)
  IpxRequest setEnoRs(int vrNumber, int value) {
    return this.setEnoVr(vrNumber, value);
  }

  IpxRequest clearRelay(int relayNumber) {
    assert (relayNumber <= 56 && relayNumber >= 0);

    queryParams["ClearR"] = relayNumber.toString().padLeft(2, "0");
    return this;
  }

  IpxRequest clearVirtualOutput(int virtualOutputNumber) {
    assert (virtualOutputNumber >= 0 && virtualOutputNumber <= 128);

    queryParams["ClearVO"] = virtualOutputNumber.toString().padLeft(3, "0");
    return this;
  }

  IpxRequest clearVirtualInput(int virtualInputNumber) {
    assert (virtualInputNumber >= 0 && virtualInputNumber <= 128);

    queryParams["ClearVI"] = virtualInputNumber.toString().padLeft(3, "0");

    return this;
  }

  IpxRequest clearEnoPC(int enoNumber) {
    assert (enoNumber > 0 && enoNumber <= 24);

    queryParams["ClearEnoPC"] = enoNumber.toString();

    return this;
  }

  IpxRequest toggleRelay(int relayNumber) {
    assert (relayNumber > 0 && relayNumber <= 56);

    queryParams["ToggleR"] = relayNumber.toString().padLeft(2, "0");

    return this;
  }

  IpxRequest toggleVirtualOutput(int virtualOutputNumber) {
    assert (virtualOutputNumber > 0 && virtualOutputNumber <= 128);

    queryParams["ToggleVO"] = virtualOutputNumber.toString().padLeft(3, "0");

    return this;
  }

  IpxRequest toggleVirtualInput(int virtualInputNumber) {
    assert (virtualInputNumber > 0 && virtualInputNumber <= 128);

    queryParams["ToggleVI"] = virtualInputNumber.toString().padLeft(3, "0");

    return this;
  }

  IpxRequest toggleEnoPC(int enoPcNumber) {
    assert (enoPcNumber > 0 && enoPcNumber <= 24);

    queryParams["ToggleEnoPC"] = enoPcNumber.toString();

    return this;
  }

  IpxRequest getDigitalInputs() {
    queryParams["Get"] = "D";
    return this;
  }

  IpxRequest getAnalogInputs() {
    queryParams["Get"] = "A";
    return this;
  }

  IpxRequest getCounters() {
    queryParams["Get"] = "C";
    return this;
  }

  IpxRequest getVirtualInputs() {
    queryParams["Get"] = "VI";
    return this;
  }

  IpxRequest getVirtualOutputs() {
    queryParams["Get"] = "VO";
    return this;
  }

  IpxRequest getVirtualAnalogInputs() {
    queryParams["Get"] = "VA";
    return this;
  }

  IpxRequest getPingWatchdogs() {
    queryParams["Get"] = "PW";
    return this;
  }

  IpxRequest getXEno() {
    queryParams["Get"] = "XENO";
    return this;
  }

  IpxRequest getXTHL() {
    queryParams["Get"] = "XTHL";
    return this;
  }

  IpxRequest getX4VR(int vrExtensionNumber) {
    queryParams["Get"] = "VR"+vrExtensionNumber.toString();
    return this;
  }

  IpxRequest getFp() {
    queryParams["Get"] = "FP";
    return this;
  }

  IpxRequest getLastSms() {
    queryParams["Get"] = "SMS";
    return this;
  }

  IpxRequest getDimmer() {
    queryParams["Get"] = "G";
    return this;
  }

  IpxRequest getT() {
    queryParams["Get"] = "T";
    return this;
  }

  IpxRequest getAll() {
    queryParams["Get"] = "all";
    return this;
  }

  Future<void> Do() async {
    queryParams["apikey"] = apikey;
    var uri = Uri.http(this.host, '/api/xdevices.json', queryParams);

    var req =  await httpClient.getUrl(uri);
    var response = await req.close();

    if (response.statusCode > 300) {
      throw IpxApiError(response.reasonPhrase, response.statusCode);
    }
  }
}


class Ipx800Client {
  String host;
  int port;
  String apikey;
  HttpClient httpClient = HttpClient();

  Ipx800Client({this.host, this.port, this.apikey});

  IpxRequest NewQuery() {
    return new IpxRequest(port:  this.port, apikey: this.apikey, host: this.host, httpClient: this.httpClient);
  }
}
